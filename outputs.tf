output "nonprod_new_vpc_id" {
  description = "This is the new VPC ID"
  value       = aws_vpc.nonprod_new_vpc.id
}

output "nonprod_new_vpc_subnet_id" {
  description = "This is the new VPC SUBNET ID"
  value       = aws_subnet.nonprod_new_vpc_subnet.id
}
