variable "name" {
  description = "This is the name to attach to your resources."
}

variable "region" {
  description = "This is the region into which you'll deploy your instance."
  default     = "us-west-1"
}

