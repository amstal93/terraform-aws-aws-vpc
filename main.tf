provider "aws" {
  region = var.region
}

# RESOURCE CONFIGURATION - AWS
resource "aws_vpc" "nonprod_new_vpc" {
  cidr_block           = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags = {
    Name = "${var.name}-vpc"
  }
}

resource "aws_subnet" "nonprod_new_vpc_subnet" {
  vpc_id                  = aws_vpc.nonprod_new_vpc.id
  cidr_block              = "10.0.0.0/24"
  map_public_ip_on_launch = "true"

  tags = {
    Name = "${var.name}-subnet"
  }

}

resource "aws_internet_gateway" "nonprod_new_igw" {
  vpc_id = aws_vpc.nonprod_new_vpc.id

  tags = {
    Name = "${var.name}-igw"
  }
}

resource "aws_route" "route_public_access" {
  route_table_id         = aws_vpc.nonprod_new_vpc.main_route_table_id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.nonprod_new_igw.id

}
